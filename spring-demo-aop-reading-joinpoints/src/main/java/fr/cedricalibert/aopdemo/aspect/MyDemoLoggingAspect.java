package fr.cedricalibert.aopdemo.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import fr.cedricalibert.aopdemo.Account;

@Aspect
@Component
@Order(2)
public class MyDemoLoggingAspect {

	@Before("fr.cedricalibert.aopdemo.aspect.AopExpressions.forDaoPackageExceptSettersAndGetters()")
	public void beforeAddAccountAdvice(JoinPoint joinPoint) {
		System.out.println("\n======> Executing @Before advice on addAccount");
		
		//display the methods signature
		MethodSignature methodSign = (MethodSignature) joinPoint.getSignature();
		
		System.out.println("Method : "+methodSign);
		
		//display method argument
		Object[] args = joinPoint.getArgs();
		
		for (Object arg : args) {
			System.out.println(arg);
			
			if(arg instanceof Account) {
				//downcast and print Account specific stuff
				Account account = (Account) arg;
				System.out.println("Account Name : "+account.getName());
				System.out.println("Account Level : "+account.getLevel());
			}
		}
		
	}
	
}
